provider "google" {
  credentials = "${file("gcp_account.json")}"
  project     = "ml-development-248518"
  region      = "us-west2"
  zone        = "us-west2-b"
}

terraform {
  backend "gcs" {
    bucket  = "vizix-ai-tf-state"
    prefix  = "ml-dev/state"
  }
}

resource "google_compute_disk" "boot" {
  name  = "vince-fastai"
  type  = "pd-ssd"
  zone  = "us-west2-b"
#  image = "deeplearning-platform-release/pytorch-latest-gpu"
  snapshot = "https://www.googleapis.com/compute/v1/projects/ml-development-248518/global/snapshots/snapshot-1"
  size  = 200

  labels = {
    environment = "dev"
  }

  physical_block_size_bytes = 4096
}

resource "google_compute_instance" "vince_fastai" {
  name         = "vince-fastai"
  machine_type = "n1-highmem-4"
  zone         = "us-west2-b"

  tags = ["vsheffer", "ml"]

  boot_disk {
    source = "${google_compute_disk.boot.self_link}"
  }

  scheduling {
    on_host_maintenance = "TERMINATE"
    preemptible         = true
    automatic_restart   = false
  }

  guest_accelerator {
    type  = "nvidia-tesla-p4"
    count = 1
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  metadata = {
    install-nvidia-driver = "True"
  }

  metadata_startup_script = "echo hi > /test.txt"

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}

#module "cloud-ep-dns" {
#  source      = "terraform-google-modules/endpoints-dns/google"
#  project     = "ml-development-248518"
#  name        = "vincefastai"
#  external_ip = "${google_compute_instance.vince_fastai.network_interface.0.access_config.0.assigned_nat_ip}"
#}
